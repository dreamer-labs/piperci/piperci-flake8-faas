from pathlib import Path

import pytest

from piperci_flake8.function.config_schema import Flake8ConfigSchema
from marshmallow.exceptions import ValidationError


@pytest.fixture()
def flake8_schema():

    return Flake8ConfigSchema()


@pytest.mark.parametrize("task_config", [
    ({"lint_paths": "a"}, 422),  # should fail not a list
    ({"lint_paths": ["a/", "b/"], "flake8_conf_path": 1234}, 422),  # not valid path
    ({"flake8_conf_path": "./flake8.conf"}, 422),  # missing lint_paths
    ({"lint_paths": [None, None]}, 422)
])
def test_flake8configschema_errors(flake8_schema, task_config):
    with pytest.raises(ValidationError):
        flake8_schema.load(task_config[0])


@pytest.mark.parametrize("task_config", [
    ({"lint_paths": ["a/", "b/"], "flake8_conf_path": "./flake8.conf"}, 200),
    ({"lint_paths": ["a/", "b/"]}, 200),
])
def test_flake8configschema_valid(flake8_schema, task_config):

    flake8_schema.load(task_config[0])


def test_load_path_field(flake8_schema):

    flake8_schema.load({"lint_paths": ["./a"], "flake8_conf_path": Path("./abc")})
    flake8_schema.load({"lint_paths": ["./a"], "flake8_conf_path": "./abc"})


def test_dump_path_field(flake8_schema):

    x = flake8_schema.load({"lint_paths": ["./a"], "flake8_conf_path": "./abc"})

    flake8_schema.dump(x)
