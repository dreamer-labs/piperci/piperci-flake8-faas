from mock import ANY
import os

import responses
import shutil
import zipfile

from piperci.storeman.client import storage_client
from piperci.task.exceptions import PiperError, PiperDelegateError
from piperci.task.this_task import ThisTask
from piperci.sri import generate_sri, hash_to_urlsafeb64

import pytest


@pytest.fixture()
def artifacts(minio_server, mocker):

    storage = storage_client(  # noqa: S106
        storage_type="minio",
        hostname=minio_server,
        access_key="MINIO_TEST_ACCESS",
        secret_key="MINIO_TEST_SECRET",
    )

    # path to testdata next to this __file__
    artifact_data = os.path.join(os.path.realpath(os.path.dirname(__file__)),
                                 "testdata")

    artifact_staging_path = os.path.join(
        os.path.realpath(os.path.join(os.environ["MODULE_ROOT"], '..')), "_artifacts")

    if os.path.exists(artifact_staging_path):
        shutil.rmtree(artifact_staging_path)

    os.mkdir(artifact_staging_path)
    artifacts = []

    container = "run-create-1"

    for artifact_path in os.listdir(artifact_data):

        artifact_path = os.path.realpath(os.path.join(artifact_data,
                                                      artifact_path))
        zipped = zip_project(artifact_path, artifact_staging_path)
        obj_name = f"artifact/unittest/{os.path.basename(zipped.filename)}"

        obj = storage.upload_file(container, obj_name, zipped.filename)
        artifacts.append({"local_file": zipped.filename,
                          "object": obj,
                          "artifact_name": os.path.basename(zipped.filename),
                          "sri": generate_sri(zipped.filename),
                          "name": artifact_path,
                          "fqdn": storage.gen_file_uri(container, obj_name)
                          })

    return artifacts


def zip_project(proj_path, destination):
    """
    Zips all files in the project and returns the zipfile
    object.
    :proj_path: The path to the project root directory
    :param destination: Path to create the zipfile in
    :return: ZipFile
    """
    zip_file = zipfile.ZipFile(
        f"{destination}/{os.path.basename(proj_path)}.zip",
        "w",
        zipfile.ZIP_DEFLATED,
    )

    for root, dirs, files in os.walk(proj_path):
        for file in files:
            zip_file.write(
                os.path.join(root, file),
                os.path.relpath(
                    os.path.join(root, file), proj_path
                ),
            )

    zip_file.close()

    return zip_file


@pytest.mark.parametrize("func_type", ["gateway", "executor"])
def test_no_json(client, func_type):
    headers = {"Content-Type": "application/json"}
    resp = client.post(f"/{func_type}", headers=headers)
    assert resp.status_code == 400


@pytest.mark.parametrize("func_type", ["gateway", "executor"])
def test_no_content_type_json(client, single_task, func_type):
    resp = client.post(f"/{func_type}", data=str(single_task))
    assert resp.status_code == 400


@pytest.mark.parametrize("task_config", [
    ({"lint_paths": ["a/", "b/"], "flake8_conf_path": "./flake8.conf"}, 200),
    ({"lint_paths": ["a/", "b/"]}, 200),
    ({"lint_paths": "a"}, 422),  # should fail not a list
    ({"lint_paths": ["a/", "b/"], "flake8_conf_path": 1234}, 422),  # not valid path
    ({"flake8_conf_path": "./flake8.conf"}, 422),  # missing lint_paths
])
@responses.activate
def test_gateway(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    single_task,
    start_task_response,
    task_artifact_upload_response,
    task_creation_response,
    task_config,
    task_post_executor_url_response
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    responses.add_callback(*task_post_executor_url_response[0],
                           **task_post_executor_url_response[1])

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    mocker.patch("piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch("piperci_flake8.function.models.gman_client", gman_client_mock())

    single_task["config"] = task_config[0]

    resp = client.post("/gateway", json=single_task, headers=common_headers)

    if task_config[1] == 200:
        assert resp.status_code == 200
        assert "config" in task_post_executor_url_response[2]["json"]
        assert task_post_executor_url_response[2]["marshaller"] is not False, (
            "failed to pass executor marshaller"
        )

    assert resp.status_code == task_config[1]


@responses.activate
def test_gateway_piper_error(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    single_task,
    start_task_response,
    task_artifact_upload_response,
    task_creation_response,
    task_post_executor_url_response
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    responses.add_callback(*task_post_executor_url_response[0],
                           **task_post_executor_url_response[1])

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
        storage_type="minio",
        hostname=minio_server,
        access_key="MINIO_TEST_ACCESS",
        secret_key="MINIO_TEST_SECRET",
    )

    class CustTask(ThisTask):

        _init_storage_client = store_cli

        storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

        def info(self, *args, **kwargs):
            if "called successfully" in args[0]:
                raise PiperError("Error for testing")
            return super().info(*args, **kwargs)

    mocker.patch("piperci_flake8.function.faas_app.ThisTask", CustTask)
    mocker.patch("piperci_flake8.function.models.gman_client", gman_client_mock())

    single_task["config"]["lint_paths"] = ["a", "b"]
    resp = client.post("/gateway", json=single_task, headers=common_headers)

    assert resp.status_code == 400


@responses.activate
def test_gateway_piper_error_delegate(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    single_task,
    start_task_response,
    task_artifact_upload_response,
    task_creation_response
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
        storage_type="minio",
        hostname=minio_server,
        access_key="MINIO_TEST_ACCESS",
        secret_key="MINIO_TEST_SECRET",
    )

    class CustTask(ThisTask):

        _init_storage_client = store_cli

        storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

        def delegate(self, *args, **kwargs):
            raise PiperDelegateError("Error for testing")

    mocker.patch("piperci_flake8.function.faas_app.ThisTask", CustTask)
    mocker.patch("piperci_flake8.function.models.gman_client", gman_client_mock())
    single_task["config"]["lint_paths"] = ["a", "b"]

    resp = client.post("/gateway", json=single_task, headers=common_headers)
    assert resp.status_code == 400


@responses.activate
def test_executor_artifacts(
    client,
    config,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    start_task_response,
    task_creation_response,
    task_artifact_upload_response,
    threaded_task,
    artifacts
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    task_config = {"lint_paths": []}
    threaded_task["artifacts"] = []

    for artifact in artifacts:
        sri_urlsafe = hash_to_urlsafeb64(artifact["sri"])

        responses.add(responses.GET,
                      f"{config['gman']['url']}/artifact/sri/{sri_urlsafe}",
                      json=[{
                        "status": "unknown",
                        "uri": artifact["fqdn"],
                        "artifact_id": "884053a3-277b-45e4-9813-fc61c07a2cd6",
                        "type": "artifact",
                        "sri": str(artifact["sri"]),
                        "task": {
                            "task_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
                            "caller": "test_case_create_1",
                            "project": "gman_test_data",
                            "thread_id": task_creation_response[1]["task"]["thread_id"],
                            "run_id": "create_1",
                        },
                        "event_id": "a48efe28-db9e-4330-93c4-5f480b2bef71",
                        }])

        threaded_task["artifacts"].append(str(artifact["sri"]))
        task_config["lint_paths"].append(f"{os.path.basename(artifact['name'])}/*.py")

    mocker.patch("piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch("piperci_flake8.function.models.gman_client", gman_client_mock())
    threaded_task["config"] = task_config

    tmp_exists = os.path.exists

    art_names = [os.path.basename(x["name"]) for x in artifacts]

    def p_exists(f_path):
        # Check that files exist at time of exec because faas_app cleans up tmpdirs
        # after running.any(iterable)
        if os.path.basename(f_path) in art_names:
            assert tmp_exists(f_path), (f"artifact {f_path} is missing when it was "
                                        "expected after artifact extraction")
        return tmp_exists(f_path)

    mocker.patch("piperci_flake8.function.handler.os.path.exists", p_exists)
    resp = client.post("/executor", json=threaded_task, headers=common_headers)

    assert resp.status_code == 200


@responses.activate
@pytest.mark.parametrize("artifact_idx", [0, 1])
def test_executor_check_command_output(
    client,
    config,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    start_task_response,
    task_creation_response,
    task_artifact_upload_response,
    threaded_task,
    artifacts,
    artifact_idx
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    task_config = {"lint_paths": []}
    threaded_task["artifacts"] = []

    artifact = artifacts[artifact_idx]
    sri_urlsafe = hash_to_urlsafeb64(artifact["sri"])

    responses.add(responses.GET,
                  f"{config['gman']['url']}/artifact/sri/{sri_urlsafe}",
                  json=[{
                    "status": "unknown",
                    "uri": artifact["fqdn"],
                    "artifact_id": "884053a3-277b-45e4-9813-fc61c07a2cd6",
                    "type": "artifact",
                    "sri": str(artifact["sri"]),
                    "task": {
                        "task_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
                        "caller": "test_case_create_1",
                        "project": "gman_test_data",
                        "thread_id": task_creation_response[1]["task"]["thread_id"],
                        "run_id": "create_1",
                    },
                    "event_id": "a48efe28-db9e-4330-93c4-5f480b2bef71",
                    }])

    threaded_task["artifacts"].append(str(artifact["sri"]))
    task_config["lint_paths"].append(f"{os.path.basename(artifact['name'])}/*.py")

    mocker.patch("piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch("piperci_flake8.function.models.gman_client", gman_client_mock())
    threaded_task["config"] = task_config

    tmp_exists = os.path.exists

    art_names = [os.path.basename(x["name"]) for x in artifacts]

    def p_exists(f_path):
        # Check that files exist at time of exec because faas_app cleans up tmpdirs
        # after running.any(iterable)
        if os.path.basename(f_path) in art_names:
            assert tmp_exists(f_path), (f"artifact {f_path} is missing when it was "
                                        "expected after artifact extraction")
        return tmp_exists(f_path)

    mocker.patch("piperci_flake8.function.handler.os.path.exists", p_exists)
    info_mock = mocker.patch("piperci.task.this_task.ThisTask.info")
    resp = client.post("/executor", json=threaded_task, headers=common_headers)

    assert resp.status_code == 200

    if "fail" in artifact["name"]:
        info_mock.assert_any_call(ANY, return_code=1)

    elif "pass" in artifact["name"]:
        info_mock.assert_any_call(ANY, return_code=0)


@responses.activate
@pytest.mark.parametrize("task_config", [
    ({"lint_paths": ["a/", "b/"], "flake8_conf_path": "a/flake8.conf"}, 200),
    ({"lint_paths": ["a/", "b/"]}, 200),
    ({"lint_paths": "a"}, 422),  # should fail not a list
    ({"lint_paths": ["a/", "b/"], "flake8_conf_path": 1234}, 422),  # not valid path
    ({"flake8_conf_path": "./flake8.conf"}, 422),  # missing lint_paths
])
def test_executor_param_validations(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    start_task_response,
    task_creation_response,
    task_artifact_upload_response,
    threaded_task,
    task_config
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    mocker.patch("piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch("piperci_flake8.function.models.gman_client", gman_client_mock())
    threaded_task["config"] = task_config[0]
    resp = client.post("/executor", json=threaded_task, headers=common_headers)

    assert resp.status_code == task_config[1]


@responses.activate
def test_executor_param_invalid_lint_path_logged(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    start_task_response,
    task_creation_response,
    task_artifact_upload_response,
    threaded_task
):
    task_config = ({"lint_paths": ["a/*.py"]}, 200)

    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    mocker.patch("piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch("piperci_flake8.function.models.gman_client", gman_client_mock())

    threaded_task["config"] = task_config[0]
    info_mock = mocker.patch("piperci.task.this_task.ThisTask.info")
    resp = client.post("/executor", json=threaded_task, headers=common_headers)

    assert resp.status_code == task_config[1]

    seen = False
    for call in info_mock.mock_calls:
        try:
            if "Lint base dir check" in call[1][0]:
                seen = True
        except Exception:
            pass

    assert seen, (f'"Lint base dir check PATHNAME does not exist"'
                  ' weas NOT called')


@responses.activate
def test_executor_piper_error(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    start_task_response,
    task_creation_response,
    task_artifact_upload_response,
    threaded_task,
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    def raise_exception(*args, **kwargs):
        raise PiperError("Error for testing")

    mocker.patch("piperci_flake8.function.handler.execute_code", raise_exception)

    mocker.patch("piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch("piperci_flake8.function.models.gman_client", gman_client_mock())
    resp = client.post("/executor", json=threaded_task, headers=common_headers)

    assert resp.status_code == 400
