import os
import random
import string
import tempfile
import sys


def test_secrets(mocker):

    secrets_path = os.path.join(tempfile.gettempdir(),
                                "pytest_"
                                + "".join(random.choices(string.ascii_lowercase, k=4)))

    class mydict_stub(dict):
        def __init__(self, dict_vals):
            if "storage" in dict_vals:
                dict_vals["storage"] = {
                    "access_key_secret": secrets_path,
                    "secret_key_secret": secrets_path,
                }
            super().__init__(dict_vals)

    with open(secrets_path, "w+") as secrets_file:
        secrets_file.write("1234")

    tmp_mod = sys.modules["piperci_flake8.function.config"]
    del sys.modules["piperci_flake8.function.config"]

    mocker.patch("builtins.dict",  mydict_stub)

    from piperci_flake8.function.config import Config

    del sys.modules["piperci_flake8.function.config"]
    sys.modules["piperci_flake8.function.config"] = tmp_mod

    assert Config["storage"]["access_key"] == "1234"


# def test_secrets_one_invalid(mocker):
#
#     secrets_path = os.path.join(tempfile.gettempdir(),
#                                 "pytest_"
#                                 + "".join(random.choices(string.ascii_lowercase, k=4)))
#
#     class mydict_stub(dict):
#         def __init__(self, dict_vals):
#             if "storage" in dict_vals:
#                 dict_vals["storage"] = {
#                     "access_key_secret": secrets_path,
#                     "secret_key_secret": "not valid",
#                 }
#             super().__init__(dict_vals)
#
#     with open(secrets_path, "w+") as secrets_file:
#         secrets_file.write("1234")
#
#     mocker.patch("builtins.dict",  mydict_stub)
#
#     from piperci_flake8.function.config import Config
#
#     assert Config["storage"]["access_key"] == "1234"
