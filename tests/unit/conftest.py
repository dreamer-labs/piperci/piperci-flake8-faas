import json
import os
import responses
import sys
import uuid

from typing import Tuple
from urllib.parse import urlparse

import pytest

try:
    sys.path.insert(0, os.environ["MODULE_ROOT"])
except KeyError:
    pass

from piperci_flake8.function.faas_app import app as papp
from piperci_flake8.function.config import Config

from piperci_flake8.function.marshaller import MarshallError
from piperci_flake8.function.task_marshaller import TaskMarshaller

parent_id = str(uuid.uuid4())
thread_id = str(uuid.uuid4())


@pytest.fixture
def app():  # required by pytest_flask
    return papp


@pytest.fixture
def config():
    return Config


@pytest.fixture
def minio_server(s3_server):
    server_url = s3_server.boto_endpoint_url
    return urlparse(server_url).netloc


@pytest.fixture
def minio_bucket_with_files(s3_bucket):
    bucket = s3_bucket.name
    object = s3_bucket.client.Object(bucket, "test")
    object.put(Body="test")
    return bucket


@pytest.fixture
def task_post_executor_url_response(config):
    introspect = {"json": {}}

    def callback(request):
        body = json.loads(request.body)
        introspect["json"] = body
        try:
            marshaller = TaskMarshaller(body)
            marshaller.enforce("pre_task")
            introspect["marshaller"] = True
            return 200, {}, json.dumps({"success": "valid json"})
        except MarshallError as e:
            introspect["marshaller"] = False
            return 422, {}, json.dumps(e.errors.emit())
        except Exception as e:
            introspect["marshaller"] = e
            return (400, {}, json.dumps({"errors": [{
                "general": f"Unknown error processing function. \nUnknown"}]})
                    )

    return (
        [responses.POST,
         config["executor_url"].format(endpoint=config["endpoint"])],
        {"callback": callback},
        introspect
        )


@pytest.fixture
def valid_configs():
    return {"lint_paths": ["./*.py"]}


@pytest.fixture
def single_task(valid_configs):
    return {"project": "A Test Project",
            "run_id": "12345",
            "stage": "tox_tests",
            "parent_id": parent_id,
            "config": valid_configs,
            "lint_paths": None}


@pytest.fixture
def threaded_task(valid_configs):

    return {
        "thread_id": thread_id,
        "parent_id": parent_id,
        "project": "A Test project",
        "run_id": "54321",
        "stage": "tox_tests",
        "config": valid_configs
    }


@pytest.fixture
def common_headers(config, request_ctx):
    config["endpoint"] = request_ctx.request.host_url
    return {"Content-Type": "application/json", "X-Forwarded-Host": config["endpoint"]}


@pytest.fixture
def start_task_response(config) -> Tuple[Tuple, dict]:
    return (
        (responses.POST, f"{config['gman']['url']}/task"),
        {
            "task": {
                "parent_id": None,
                "run_id": "create_1",
                "project": "gman_test_data",
                "thread_id": thread_id,
                "caller": "test_case_create_1",
                "task_id": thread_id,
            }
        },
    )


@pytest.fixture
def task_creation_response(config) -> Tuple[Tuple, dict]:
    return (
        (
            responses.PUT,
            f"{config['gman']['url']}/task/{thread_id}",
        ),
        {
            "timestamp": "2019-07-17T12:10:32.952267+00:00",
            "event_id": "9038675b-66ad-4d8f-9c38-a3fa5a93db8c",
            "task": {
                "parent_id": None,
                "run_id": "create_1",
                "project": "gman_test_data",
                "thread_id": thread_id,
                "caller": "test_case_create_1",
                "task_id": thread_id,
            },
            "return_code": None,
            "status": "started",
            "message": " task creation body",
        },
    )


@pytest.fixture
def task_artifact_upload_response(config) -> Tuple[Tuple, dict]:
    return (
        (responses.POST, f"{config['gman']['url']}/artifact"),
        {
            "status": "unknown",
            "uri": "https://someminio.example.com/art1",
            "artifact_id": "884053a3-277b-45e4-9813-fc61c07a2cd6",
            "type": "artifact",
            "sri": "sha256-sCDaaxdshXhK4sA/v4dMHiMWhtGyQwA1fP8PgrN0O5g=",
            "task": {
                "task_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
                "caller": "test_case_create_1",
                "project": "gman_test_data",
                "thread_id": thread_id,
                "run_id": "create_1",
            },
            "event_id": "a48efe28-db9e-4330-93c4-5f480b2bef71",
        },
    )


@pytest.fixture
def gman_client_mock():

    class _gman_mock(object):

        def get_run(*args, **kwargs):
            return kwargs["run_id"]

        def get_thread(*args, **kwargs):
            return kwargs["thread_id"]

        def get_task(*args, **kwargs):
            return kwargs["task_id"]

    return _gman_mock
