## [1.0.1](https://gitlab.com/dreamer-labs/piperci/piperci-flake8-faas/compare/v1.0.0...v1.0.1) (2020-01-31)


### Bug Fixes

* fixed delegate error handling ([a572cc1](https://gitlab.com/dreamer-labs/piperci/piperci-flake8-faas/commit/a572cc1))

# 1.0.0 (2020-01-27)


### Features

* flake8 execution and config_schema, coverage ([a4a4104](https://gitlab.com/dreamer-labs/piperci/piperci-flake8-faas/commit/a4a4104))
