
Config = dict({
    "gman": {"url": "http://gman:8080"},
    "storage": {
        "url": "minio:9000",
        "access_key_secret": "/var/openfaas/secrets/access-key",
        "secret_key_secret": "/var/openfaas/secrets/secret-key",
    },
    "name": "flake8",
    "executor_url": "http://gateway:8080/async-function/piperci-flake8/executor",
    "local_artifact_path": "/tmp/artifact",
    "custom_schema_module": "config_schema",
    "custom_schema_class": "Flake8ConfigSchema"
})

try:
    with open(Config["storage"]["access_key_secret"], "r") as access_key_file:
        Config["storage"]["access_key"] = access_key_file.readline().strip("\n")

    with open(Config["storage"]["secret_key_secret"], "r") as access_key_file:
        Config["storage"]["secret_key"] = access_key_file.readline().strip("\n")
except (KeyError, IOError):
    Config["storage"]["access_key"] = "test"
    Config["storage"]["secret_key"] = "test_secret"
