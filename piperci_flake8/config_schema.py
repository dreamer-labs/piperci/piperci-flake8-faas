from pathlib import Path

from marshmallow import fields, Schema, EXCLUDE


class PathField(fields.String):

    def _validated(self, value):

        if isinstance(value, Path):
            return value

        try:
            return Path(value)
        except (TypeError, ValueError) as error:
            raise self.make_error("invalid", input=value) from error

    def _serialize(self, value, attr, obj, **kwargs):
        return str(value) if value is not None else None

    def _deserialize(self, value, attr, data, **kwargs):
        return self._validated(value)


class Flake8ConfigSchema(Schema):

    flake8_conf_path = PathField(required=False,
                                 allow_none=True,
                                 missing=None,
                                 default=None)

    lint_paths = fields.List(PathField(required=True),
                             required=True)

    class Meta:
        unknown = EXCLUDE
