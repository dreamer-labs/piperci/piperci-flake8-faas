import os
import subprocess

from flask import Request
from typing import Dict, Tuple, Union

from piperci.task.exceptions import PiperDelegateError, PiperError
from piperci.task.this_task import ThisTask

# from .config_schema import Flake8ConfigSchema
from .models import task_schema_gen


def executor(
    request: Request, task: ThisTask, config: dict
) -> Tuple[Union[Dict, str], int]:
    """
    Entrypoint to the flake8 task async executor function.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this task
    :param config: FaaS config object
    :return: (response_body, code)
    """
    task.info(str(task.task))
    # See piperci.task.this_task for gman.task interface usage docs
    try:
        task.info("flake8 task gateway handler.executor called successfully")
        task.info("fetching artifacts")
        artifacts = []

        for artifact in config["task_config"]["artifacts"]:
            art_data = task.get_source(artifact=artifact)
            if "zip" in art_data["extract_to"][-4:]:  # Rename .zip to artifact dir
                os.rename(art_data["extract_to"], art_data["extract_to"][:-4])
                art_data["extract_to"] = art_data["extract_to"][:-4]
            artifacts.append(art_data)

        execute_code(request, task, config["task_config"]["config"], artifacts)

        return task.complete("flake8 task complete")
    except PiperError as e:
        return task.fail(str(e))


def execute_code(request: Request, task: ThisTask, config: dict, artifacts: list):
    # Replace below with task execution logic

    if config["flake8_conf_path"]:
        flake8_conf_path = os.path.realpath(config["flake8_conf_path"])
        task.info(f"Executing flake8 config file {flake8_conf_path}")
        config_file = ["--config", flake8_conf_path]
    else:
        config_file = []

    for path in config["lint_paths"]:
        path = os.path.realpath(path)
        if not os.path.exists(os.path.dirname(path)):
            task.info(f"Lint base dir check {os.path.dirname(path)} does not exist. "
                      "Check your lint_paths arguments or that the artifacts exist.")

    called_p = subprocess.run(["flake8"] + config_file + config["lint_paths"],
                              shell=True,
                              capture_output=True,
                              text=True)

    task.stderr(called_p.stderr)
    task.stdout(called_p.stdout)

    task.info(f"Command exited with return code {called_p.returncode}",
              return_code=called_p.returncode)


def gateway(request: Request, task: ThisTask, config: dict):
    """
    Entrypoint to the flake8 Task.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this task
    :param config: FaaS config object
    :return: (response_body, code)
    """

    try:
        task.info("flake8 task gateway handler.handle called successfully")
    except PiperError as e:
        return {"errors": {"flake8.handler:gateway"
                           [f"{str(e)}: no delegate was attempted"]}}, 400

    try:
        schema = task_schema_gen()
        task.delegate(config["executor_url"], data=schema().dump(config["task_config"]))
        return task.complete("flake8 gateway completed successfully")
    except PiperDelegateError as e:
        return task.fail(str(e))
